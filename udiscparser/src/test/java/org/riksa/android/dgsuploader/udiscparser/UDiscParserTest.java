package org.riksa.android.dgsuploader.udiscparser;

import junit.framework.TestCase;

import org.joda.time.DateTime;
import org.junit.Test;
import org.riksa.android.dgsuploader.factory.PlayerFactory;
import org.riksa.android.dgsuploader.factory.RoundFactory;
import org.riksa.android.dgsuploader.model.Round;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by riksa on 18/10/15.
 */
public class UDiscParserTest extends TestCase {

    public void setUp() throws Exception {
        super.setUp();

    }

    public void tearDown() throws Exception {

    }

    @Test
    public void testParse1() throws Exception, ParseException {
        final File file = new File("samplefiles/sample1.csv");
        assertTrue("File not found " + file.getAbsolutePath(), file.exists());
        final RoundFactory roundFactory = mock(RoundFactory.class);
        final PlayerFactory playerFactory = mock(PlayerFactory.class);

        final UDiscParser uDiscParser = new UDiscParser(roundFactory, playerFactory);
        final FileInputStream inputStream = new FileInputStream(file);

        final Round roundMock = mock(Round.class);

        final DateTime value = new DateTime(2015, 9, 30, 17, 45);
        when(roundFactory.newInstance("Nokia DiscGolfPark", "New Layout", 12, 37, value))
                .thenReturn(roundMock);

        final Round round = uDiscParser.parse(inputStream);
        inputStream.close();

        final List<Integer> rikuResults = Arrays.asList(4, 4, 3, 5, 3, 4, 4, 4, 3, 4, 4, 3);
        final List<Integer> janiResults = Arrays.asList(4, 4, 4, 5, 3, 4, 4, 4, 4, 3, 3, 2);
        final List<Integer> juhaResults = Arrays.asList(3, 5, 4, 4, 6, 4, 4, 3, 4, 3, 3, 2);
        final List<Integer> mattiResults = Arrays.asList(3, 4, 3, 5, 4, 4, 4, 6, 3, 2, 3, 3);

        assertThat(round, is(roundMock));
        verify(roundMock).addPlayer("Riku", rikuResults);
        verify(roundMock).addPlayer("Juha", juhaResults);
        verify(roundMock).addPlayer("Jani", janiResults);
        verify(roundMock).addPlayer("Matti", mattiResults);
    }

    @Test
    public void testParse2() throws Exception, ParseException {
        final File file = new File("samplefiles/sample2.csv");
        assertTrue("File not found " + file.getAbsolutePath(), file.exists());

        final RoundFactory roundFactory = mock(RoundFactory.class);
        final Round roundMock = mock(Round.class);

        final DateTime value = new DateTime(2015, 7, 25, 8, 33);
        when(roundFactory.newInstance("Järva Discgolf Park", "Default Layout", 27, 81, value))
                .thenReturn(roundMock);

        final PlayerFactory playerFactory = mock(PlayerFactory.class);

        final UDiscParser uDiscParser = new UDiscParser(roundFactory, playerFactory);
        final FileInputStream inputStream = new FileInputStream(file);
        final Round round = uDiscParser.parse(inputStream);
        inputStream.close();

        assertThat(round, is(roundMock));
        verify(roundFactory).newInstance("Järva Discgolf Park", "Default Layout", 27, 81, value);
        final List<Integer> rikuResults = Arrays.asList(3, 4, 3, 4, 3, 4, 6, 4, 3, 2, 4, 4, 4, 4, 2, 3, 5, 3, 3, 4, 4, 6, 4, 6, 5, 3, 3);
        final List<Integer> janiResults = Arrays.asList(3, 4, 3, 4, 4, 4, 6, 5, 5, 3, 3, 4, 4, 4, 4, 3, 6, 4, 3, 4, 3, 4, 3, 4, 4, 4, 3);
        final List<Integer> margitResults = Arrays.asList(8, 7, 4, 5, 4, 5, 9, 6, 5, 5, 5, 4, 5, 6, 4, 5, 9, 6, 6, 6, 4, 6, 6, 6, 10, 6, 6);
        final List<Integer> tainaResults = Arrays.asList(5, 6, 4, 5, 4, 7, 10, 8, 4, 3, 4, 4, 6, 6, 4, 7, 8, 5, 5, 7, 4, 6, 6, 10, 6, 4, 6);
        final List<Integer> jussiResults = Arrays.asList(4, 4, 3, 3, 3, 4, 7, 4, 4, 3, 4, 3, 4, 5, 3, 4, 5, 3, 3, 5, 4, 3, 3, 6, 4, 4, 3);
        final List<Integer> samiResuts = Arrays.asList(3, 4, 3, 3, 3, 3, 5, 5, 3, 3, 3, 4, 3, 4, 2, 3, 6, 3, 3, 3, 4, 4, 2, 4, 5, 3, 2);

        verify(roundMock).addPlayer("Riku", rikuResults);
        verify(roundMock).addPlayer("Margit", margitResults);
        verify(roundMock).addPlayer("Jani", janiResults);
        verify(roundMock).addPlayer("Taina", tainaResults);
        verify(roundMock).addPlayer("Jussi", jussiResults);
        verify(roundMock).addPlayer("Sami", samiResuts);
    /*

PlayerName, Hole1, Hole2, Hole3, Hole4, Hole5, Hole6, Hole7, Hole8, Hole9, Hole10, Hole11, Hole12, Hole13, Hole14, Hole15, Hole16, Hole17, Hole18, Hole19, Hole20, Hole21, Hole22, Hole23, Hole24, Hole25, Hole26, Hole27, Out, +/-, CourseName, LayoutName, Date, Notes
Par, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 81, E, Järva Discgolf Park, Default Layout, 2015-07-25 08:33 am
Riku, 3, 4, 3, 4, 3, 4, 6, 4, 3, 2, 4, 4, 4, 4, 2, 3, 5, 3, 3, 4, 4, 6, 4, 6, 5, 3, 3, 103, 22
Margit, 8, 7, 4, 5, 4, 5, 9, 6, 5, 5, 5, 4, 5, 6, 4, 5, 9, 6, 6, 6, 4, 6, 6, 6, 10, 6, 6, 158, 77
Jani, 3, 4, 3, 4, 4, 4, 6, 5, 5, 3, 3, 4, 4, 4, 4, 3, 6, 4, 3, 4, 3, 4, 3, 4, 4, 4, 3, 105, 24
Taina, 5, 6, 4, 5, 4, 7, 10, 8, 4, 3, 4, 4, 6, 6, 4, 7, 8, 5, 5, 7, 4, 6, 6, 10, 6, 4, 6, 154, 73
Jussi, 4, 4, 3, 3, 3, 4, 7, 4, 4, 3, 4, 3, 4, 5, 3, 4, 5, 3, 3, 5, 4, 3, 3, 6, 4, 4, 3, 105, 24
Sami, 3, 4, 3, 3, 3, 3, 5, 5, 3, 3, 3, 4, 3, 4, 2, 3, 6, 3, 3, 3, 4, 4, 2, 4, 5, 3, 2, 93, 12
     */
//        assertEquals(
//                Arrays.asList(3, 4, 3, 4, 3, 4, 6, 4, 3, 2, 4, 4, 4, 4, 2, 3, 5, 3, 3, 4, 4, 6, 4, 6, 5, 3, 3),
//                round.getResultsOf("Riku"));
//        assertEquals(Arrays.asList(3, 4, 3, 4, 4, 4, 6, 5, 5, 3, 3, 4, 4, 4, 4, 3, 6, 4, 3, 4, 3, 4, 3, 4, 4, 4, 3), round.getResultsOf("Jani"));
//        assertEquals(Arrays.asList(8, 7, 4, 5, 4, 5, 9, 6, 5, 5, 5, 4, 5, 6, 4, 5, 9, 6, 6, 6, 4, 6, 6, 6, 10, 6, 6), round.getResultsOf("Margit"));
//        assertEquals(Arrays.asList(5, 6, 4, 5, 4, 7, 10, 8, 4, 3, 4, 4, 6, 6, 4, 7, 8, 5, 5, 7, 4, 6, 6, 10, 6, 4, 6), round.getResultsOf("Taina"));
//        assertEquals(Arrays.asList(4, 4, 3, 3, 3, 4, 7, 4, 4, 3, 4, 3, 4, 5, 3, 4, 5, 3, 3, 5, 4, 3, 3, 6, 4, 4, 3), round.getResultsOf("Jussi"));
//        assertEquals(Arrays.asList(3, 4, 3, 3, 3, 3, 5, 5, 3, 3, 3, 4, 3, 4, 2, 3, 6, 3, 3, 3, 4, 4, 2, 4, 5, 3, 2), round.getResultsOf("Sami"));

    }
}