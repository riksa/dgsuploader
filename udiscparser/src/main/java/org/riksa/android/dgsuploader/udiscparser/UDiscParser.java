package org.riksa.android.dgsuploader.udiscparser;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.riksa.android.dgsuploader.factory.PlayerFactory;
import org.riksa.android.dgsuploader.factory.RoundFactory;
import org.riksa.android.dgsuploader.model.Round;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class UDiscParser {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("YYYY-MM-dd KK:mm a");
    private static final CSVFormat csvFormat = CSVFormat.DEFAULT
            .withHeader()
            .withIgnoreSurroundingSpaces(true);

    private final RoundFactory roundFactory;
    private final PlayerFactory playerFactory;

    public UDiscParser(RoundFactory roundFactory, PlayerFactory playerFactory) {
        this.roundFactory = roundFactory;
        this.playerFactory = playerFactory;
    }

    /*

    PlayerName, Hole1, Hole2, Hole3, Hole4, Hole5, Hole6, Hole7, Hole8, Hole9, Hole10, Hole11, Hole12, Out, +/-, CourseName, LayoutName, Date, Notes
    Par, 3, 3, 3, 4, 3, 3, 3, 3, 3, 3, 3, 3, 37, E, Nokia DiscGolfPark, New Layout, 2015-09-30 05:45 pm
    Riku, 4, 4, 3, 5, 3, 4, 4, 4, 3, 4, 4, 3, 45, 8
    Jani, 4, 4, 4, 5, 3, 4, 4, 4, 4, 3, 3, 2, 44, 7
    Juha, 3, 5, 4, 4, 6, 4, 4, 3, 4, 3, 3, 2, 45, 8
    Matti, 3, 4, 3, 5, 4, 4, 4, 6, 3, 2, 3, 3, 44, 7



         */
    public Round parse(InputStream inputStream) throws IOException, ParseException {

        BufferedReader bufferedReader = null;
        CSVParser csvParser = null;


        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            csvParser = csvFormat.parse(bufferedReader);

            final Iterator<CSVRecord> iterator = csvParser.iterator();
            if (!iterator.hasNext()) {
                throw new ParseException("Malformed CSV file");
            }

            final CSVRecord header = iterator.next();

            final String courseName = header.get("CourseName");
            final String layoutName = header.get("LayoutName");
            final int out = Integer.parseInt(header.get("Out"));
            final DateTime dateTime = DateTime.parse(header.get("Date"), dateTimeFormatter);
            int holes = 0;
            for (; header.isMapped("Hole" + (holes + 1)); holes++) {

            }

            final Round round = roundFactory.newInstance(courseName, layoutName, holes, out, dateTime);

            while (iterator.hasNext()) {
                final CSVRecord playerRow = iterator.next();
                final String playerName = playerRow.get("PlayerName");

                final List<Integer> results = new LinkedList<>();
                for (int hole = 1; hole <= holes; hole++) {
                    results.add(Integer.parseInt(playerRow.get("Hole" + hole)));
                }
                round.addPlayer(playerName, results);

            }


            return round;
        } catch (IOException e) {
            throw new ParseException(e.getMessage());
        } catch (NumberFormatException e) {
            throw new ParseException(e.getMessage());
        } finally {
            if (csvParser != null) {
                csvParser.close();
            }
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
    }
}
