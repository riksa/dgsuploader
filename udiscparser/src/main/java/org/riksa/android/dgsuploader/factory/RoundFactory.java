package org.riksa.android.dgsuploader.factory;

import org.joda.time.DateTime;
import org.riksa.android.dgsuploader.model.Round;

/**
 * Created by riksa on 25/10/15.
 */
public interface RoundFactory {
    Round newInstance(String courseName, String layoutName, int holes, int out, DateTime dateTime);
}
