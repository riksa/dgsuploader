package org.riksa.android.dgsuploader.udiscparser;

/**
 * Created by riksa on 18/10/15.
 */
public class ParseException extends Throwable {
    public ParseException(String man) {
        super(man);
    }
}
