package org.riksa.android.dgsuploader.model;

import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by riksa on 18/10/15.
 * <p/>
 * PlayerName, Hole1, Hole2, Hole3, Hole4, Hole5, Hole6, Hole7, Hole8, Hole9, Hole10, Hole11, Hole12, Out, +/-, CourseName, LayoutName, Date, Notes
 * Par, 3, 3, 3, 4, 3, 3, 3, 3, 3, 3, 3, 3, 37, E, Nokia DiscGolfPark, New Layout, 2015-09-30 05:45 pm
 * Riku, 4, 4, 3, 5, 3, 4, 4, 4, 3, 4, 4, 3, 45, 8
 * Jani, 4, 4, 4, 5, 3, 4, 4, 4, 4, 3, 3, 2, 44, 7
 * Juha, 3, 5, 4, 4, 6, 4, 4, 3, 4, 3, 3, 2, 45, 8
 * Matti, 3, 4, 3, 5, 4, 4, 4, 6, 3, 2, 3, 3, 44, 7
 */
public interface Round {
    void addPlayer(String playerName, List<Integer> results);

    List<String> getPlayers();

    String getCourseName();

    String getLayoutName();

    int getOut();

    DateTime getDateTime();

    List<Integer> getResultsOf(String playerName);

    void addResults(String playerName, List<Integer> results);

    int getHoles();
}
