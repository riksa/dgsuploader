package org.riksa.android.dgsuploader.dao;

import org.riksa.android.dgsuploader.model.Round;

import java.util.Collection;

/**
 * Created by riksa on 25/10/15.
 */
public interface RoundsDao {
    Collection<Round> list();
}
