package org.riksa.android.dgsuploader.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import org.riksa.android.dgsuploader.BR;
import org.riksa.android.dgsuploader.dao.RoundsDao;
import org.riksa.android.dgsuploader.databinding.RoundsGridItemBinding;
import org.riksa.android.dgsuploader.model.Round;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by riksa on 25/10/15.
 */
public class RoundsAdapter extends RecyclerView.Adapter<RoundsAdapter.ViewHolder> {
    private final RoundsDao roundsDao;
    private final List<Round> rounds = new ArrayList<>();

    public RoundsAdapter(RoundsDao roundsDao) {
        this.roundsDao = roundsDao;
        reloadRounds();
    }

    private void reloadRounds() {
        synchronized (rounds) {
            rounds.clear();
            rounds.addAll(roundsDao.list());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        RoundsGridItemBinding binding = RoundsGridItemBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.binding.setRound(rounds.get(position));
    }

    @Override
    public int getItemCount() {
        return rounds.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final RoundsGridItemBinding binding;

        public ViewHolder(RoundsGridItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
