package org.riksa.android.dgsuploader.recyclerview;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import org.riksa.android.dgsuploader.BR;
import org.riksa.android.dgsuploader.R;
import org.riksa.android.dgsuploader.databinding.MappingLineItemBinding;
import org.riksa.android.dgsuploader.model.PlayerMapper;

import butterknife.ButterKnife;

/**
 * Created by riksa on 18/10/15.
 */
public class PlayerMapperAdapter extends RecyclerView.Adapter<PlayerMapperAdapter.ViewHolder> {
    private final PlayerMapper playerMapper;
    private AlertDialog alertDialog;

    public PlayerMapperAdapter(PlayerMapper playerMapper) {
        this.playerMapper = playerMapper;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        MappingLineItemBinding binding = MappingLineItemBinding.inflate(layoutInflater, parent, false);
        return new ViewHolder(binding);
    }

    private interface EnterStringCallback {
        void onEnterString(String value);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.binding.setVariable(BR.mapping, playerMapper.getItem(position));
        holder.binding.setOnClickEmail(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEntryDialog(
                        v.getContext(),
                        R.string.hint_email,
                        holder.binding.getMapping().getEmail(),
                        new EnterStringCallback() {
                            @Override
                            public void onEnterString(String value) {
                                holder.binding.getMapping().setEmail(value);
                            }
                        });

            }
        });

        holder.binding.setOnClickUserId(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEntryDialog(
                        v.getContext(),
                        R.string.hint_userId,
                        holder.binding.getMapping().getUserId(),
                        new EnterStringCallback() {
                            @Override
                            public void onEnterString(String value) {
                                holder.binding.getMapping().setUserId(value);
                            }
                        });
            }
        });
    }

    private void showEntryDialog(Context context, int titleId, String value, final EnterStringCallback callback) {
        if (alertDialog != null) {
            alertDialog.cancel();
            alertDialog = null;
        }

        final View view = LayoutInflater.from(context).inflate(R.layout.alert_dialog_text_input, null);
        final EditText editText = ButterKnife.findById(view, R.id.edit_text);
        editText.setText(value);
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(titleId)
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onEnterString(editText.getText().toString());
                        dialog.cancel();
                    }
                })
                .setNegativeButton(android.R.string.cancel, null);
        alertDialog = builder.show();
    }

    @Override
    public int getItemCount() {
        return playerMapper.getItemCount();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final MappingLineItemBinding binding;

        public ViewHolder(MappingLineItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
