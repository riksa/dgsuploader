package org.riksa.android.dgsuploader.dagger;

import org.joda.time.DateTime;
import org.riksa.android.dgsuploader.dao.RoundsDao;
import org.riksa.android.dgsuploader.model.Mapping;
import org.riksa.android.dgsuploader.model.PlayerMapper;
import org.riksa.android.dgsuploader.model.Round;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by riksa on 18/10/15.
 */
@Module
public class VariantModule {

    @Singleton
    @Provides
    public PlayerMapper providePlayerMapper(List<Mapping> playerMappingList) {
        return new PlayerMapper(playerMappingList);
    }

    @Singleton
    @Provides
    public List<Mapping> provideMappingList() {
        List<Mapping> ret = new ArrayList<>(100);
        for (int c = 1; c < 100; c++) {
            ret.add(new Mapping("User " + c, String.valueOf(c + 100), String.format("user%d@foobar.fom", c)));
        }

        return ret;
    }

    @Singleton
    @Provides
    public RoundsDao provideRoundsDao() {
        return new RoundsDao() {
            @Override
            public Collection<Round> list() {
                final List<Round> ret = new ArrayList<>(100);
                final DateTime now = new DateTime();

                for (int c = 1; c < 100; c++) {
                    ret.add(new Round(
                            "Course " + c,
                            "Layout " + c,
                            c,
                            c * 3,
                            now.minusHours(c)));
                }

                return ret;
            }
        };

    }
}
