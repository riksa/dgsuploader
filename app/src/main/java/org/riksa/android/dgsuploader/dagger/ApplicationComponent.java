/*
 *  Copyright (C) 2015 Riku Salkia <riksa@iki.fi>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package org.riksa.android.dgsuploader.dagger;

import org.riksa.android.dgsuploader.DGSApplication;
import org.riksa.android.dgsuploader.MainActivity;
import org.riksa.android.dgsuploader.MainActivityFragment;
import org.riksa.android.dgsuploader.PlayerMappingFragment;
import org.riksa.android.dgsuploader.RoundListFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by riksa on 18/08/15.
 */
@Singleton
@Component(modules = {AndroidModule.class, VariantModule.class})
public interface ApplicationComponent {
    void inject(DGSApplication application);

    void inject(MainActivity activity);

    void inject(MainActivityFragment fragment);

    void inject(PlayerMappingFragment fragment);

    void inject(RoundListFragment fragment);
}
