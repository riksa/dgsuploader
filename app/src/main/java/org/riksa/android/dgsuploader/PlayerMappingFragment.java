package org.riksa.android.dgsuploader;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.riksa.android.dgsuploader.event.RoundListEvent;
import org.riksa.android.dgsuploader.model.PlayerMapper;
import org.riksa.android.dgsuploader.recyclerview.PlayerMapperAdapter;

import javax.inject.Inject;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlayerMappingFragment extends Fragment {
    @Inject
    protected Bus bus;

    @Inject
    protected PlayerMapper playerMapper;

    public PlayerMappingFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DGSApplication.getApplicationComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_player_mapping, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = ButterKnife.findById(view, R.id.recycler_view);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.mapping_grid_span));
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.Adapter adapter = new PlayerMapperAdapter(playerMapper);
        recyclerView.setAdapter(adapter);

    }

    @Subscribe
    public void onShareRoundEvent(RoundListEvent event) {
        Timber.d("ShareRoundEvent %s", event);

    }
}
