package org.riksa.android.dgsuploader.model;

import java.util.List;

/**
 * Created by riksa on 18/10/15.
 */
public class PlayerMapper {
    private final List<Mapping> playerMappingList;

    public PlayerMapper(List<Mapping> playerMappingList) {

        this.playerMappingList = playerMappingList;
    }

    public int getItemCount() {
        return playerMappingList.size();
    }

    public Mapping getItem(int position) {
        return playerMappingList.get(position);
    }
}
