package org.riksa.android.dgsuploader;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.common.collect.ImmutableList;
import com.squareup.otto.Bus;
import com.squareup.otto.Produce;

import org.riksa.android.dgsuploader.event.RoundListEvent;
import org.riksa.android.dgsuploader.model.Round;
import org.riksa.android.dgsuploader.udiscparser.ParseException;
import org.riksa.android.dgsuploader.udiscparser.UDiscParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {
    @Inject
    protected Bus bus;


    private final List<Round> rounds = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DGSApplication.getApplicationComponent().inject(this);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        handleIntent(getIntent());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ViewPager viewPager = ButterKnife.findById(this, R.id.view_pager);
        TabLayout tabLayout = ButterKnife.findById(this, R.id.sliding_tabs);

        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    protected void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (intent == null)
            return;
        if (!Intent.ACTION_SEND.equals(intent.getAction()))
            return;

        Timber.d("handleIntent %s" + intent);
        if (intent.getExtras() != null) {
            Uri uri = (Uri) intent.getExtras().get(Intent.EXTRA_STREAM);
            InputStream inputStream = null;
            try {
                inputStream = getContentResolver().openInputStream(uri);
                UDiscParser uDiscParser = new UDiscParser();
                Round round = uDiscParser.parse(inputStream);
                rounds.add(round);
                Timber.d("Parsed %s", round);
                Timber.d("parsed round %s", round);

            } catch (IOException e) {
                Timber.e(e, e.getMessage());
            } catch (ParseException e) {
                Timber.e(e, e.getMessage());
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        Timber.e(e, e.getMessage());
                    }
                }
            }


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Produce
    public RoundListEvent provideRoundsList() {
        return new RoundListEvent(rounds);
    }

    private class MyPagerAdapter extends FragmentStatePagerAdapter {
        public MyPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                default:
                case 0:
                    return new RoundListFragment();
                case 1:
                    return new PlayerMappingFragment();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                default:
                case 0:
                    return getString(R.string.tab_title_rounds);
                case 1:
                    return getString(R.string.tab_title_mapping);
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
