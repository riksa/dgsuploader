package org.riksa.android.dgsuploader.dagger;

import android.app.Application;

import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by riksa on 18/10/15.
 */
@Module
public class AndroidModule {
    private final Application application;

    public AndroidModule(Application application) {
        this.application = application;
    }


    @Singleton
    @Provides
    public Bus provideBus() {
        return new Bus();
    }
}
