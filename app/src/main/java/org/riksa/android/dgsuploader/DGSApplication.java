package org.riksa.android.dgsuploader;

import android.app.Application;

import org.riksa.android.dgsuploader.dagger.AndroidModule;
import org.riksa.android.dgsuploader.dagger.ApplicationComponent;
import org.riksa.android.dgsuploader.dagger.DaggerApplicationComponent;
import org.riksa.android.dgsuploader.dagger.VariantModule;

import timber.log.Timber;

/**
 * Created by riksa on 18/10/15.
 */
public class DGSApplication extends Application {
    private static ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        component = DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .variantModule(new VariantModule())
                .build();

        component.inject(this);

    }

    public static ApplicationComponent getApplicationComponent() {
        return component;
    }

}
