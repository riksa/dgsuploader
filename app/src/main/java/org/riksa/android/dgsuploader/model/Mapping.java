package org.riksa.android.dgsuploader.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import org.riksa.android.dgsuploader.BR;

/**
 * Created by riksa on 18/10/15.
 */
public class Mapping extends BaseObservable {
    private final String username;
    private String userId;
    private String email;

    public Mapping(String username, String userId, String email) {
        this.username = username;
        this.userId = userId;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    @Bindable
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        notifyPropertyChanged(BR.userId);
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

}
