package org.riksa.android.dgsuploader.event;

import org.riksa.android.dgsuploader.model.Round;

/**
 * Created by riksa on 18/10/15.
 */
public class ShareRoundEvent {
    public final Round round;

    public ShareRoundEvent(Round round) {
        this.round = round;
    }
}
