package org.riksa.android.dgsuploader.event;

import org.riksa.android.dgsuploader.model.Round;

import java.util.List;

/**
 * Created by riksa on 18/10/15.
 */
public class RoundListEvent {
    public final List<Round> rounds;

    public RoundListEvent(List<Round> rounds) {
        this.rounds = rounds;
    }
}
